package com.kyriba.qa.ddt;

import com.kyriba.qa.ddt.processtemplate.TestStatus;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.util.Assert;

public class ProcessTemplateTest implements Executable {
    private final TestStatus status;

    public ProcessTemplateTest(TestStatus status) {
        this.status = status;
    }

    @Override
    public void execute() throws Throwable {
        Assert.isTrue(status.isValid(), "Found regression on " + status.getPt().getName());
    }
}
