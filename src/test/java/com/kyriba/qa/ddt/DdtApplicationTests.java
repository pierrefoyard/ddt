package com.kyriba.qa.ddt;

import com.kyriba.qa.ddt.processtemplate.TestStatus;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.junit.jupiter.api.function.Executable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@SpringBootTest
@ComponentScan("com.kyriba.qa.ddt")
class DdtApplicationTests {
	@Autowired
	private Workflow workflow;

	@TestFactory
	Collection<DynamicTest> dynamicTestsWithCollection() {
		// Create a dynamic list of tests (see https://www.baeldung.com/junit5-dynamic-tests)
		List<TestStatus> comparisonResult = workflow.run();
		return comparisonResult.stream()
				.map(status -> {
					Executable test = new ProcessTemplateTest(status);
					return DynamicTest.dynamicTest("Add test", test);
				})
				.collect(Collectors.toList());
	}

}
