package com.kyriba.qa.ddt;

import com.kyriba.qa.ddt.processtemplate.ProcessTemplate;
import com.kyriba.qa.ddt.processtemplate.ProcessTemplatesLoader;
import com.kyriba.qa.ddt.processtemplate.ProcessTemplateTester;
import com.kyriba.qa.ddt.processtemplate.TestStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class Workflow {
    private final ProcessTemplatesLoader loader;

    List<TestStatus> run() {
        // Retrieve list of process templates
        List<ProcessTemplate> pts = loader.getProcessTemplates();

        // Create a test for each process template
        List<CompletableFuture<TestStatus>> futures = pts.stream().map(pt -> {
            ProcessTemplateTester test = new ProcessTemplateTester(pt);
            return test.execute();
        }).collect(Collectors.toList());

        // Wait for end of Process Templates and return list of statuses
        return futures.stream()
                .map(CompletableFuture::join)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

}
