package com.kyriba.qa.ddt.processtemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProcessTemplatesLoader {
    private static final int MAX_PTS = 20;
    private List<ProcessTemplate> pts = new ArrayList<>();

    @Autowired
    private void init() {
        for (int i = 0; i < MAX_PTS; ++i) {
            pts.add(new ProcessTemplate("Process Template" + i));
        }
    }

    public List<ProcessTemplate> getProcessTemplates() {
        return pts;
    }
}
