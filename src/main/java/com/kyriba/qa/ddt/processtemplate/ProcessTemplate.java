package com.kyriba.qa.ddt.processtemplate;

import lombok.Data;

@Data
public class ProcessTemplate {
    private final String name;
    private final String baselineUrl;

    public ProcessTemplate(String name) {
        this.name = name;
        this.baselineUrl = "";
    }

    public int getBaselineContent() {
        // Fake value, should be enriched by baseline
        return 0;
    }
}
