package com.kyriba.qa.ddt.processtemplate;

import lombok.RequiredArgsConstructor;

import java.util.Random;
import java.util.concurrent.CompletableFuture;

import static java.lang.Thread.sleep;

@RequiredArgsConstructor
public class ProcessTemplateTester {
    private final ProcessTemplate pt;
    private final Random random = new Random();

    public CompletableFuture<TestStatus> execute() {
        TestStatus status = new TestStatus(pt);

        // -Line1: Trigger process template and retrieve pid
        // -Line2: Wait for end of process template by doing regular pooling
        // -Line3: Retrieve process template result
        // -Line4: Compare found result with baseline
        CompletableFuture<TestStatus> result = runProcessTemplate(status)
                .thenCompose(x -> poolUntilOfProcessTemplate(x))
                .thenCompose(x -> loadProcessTemplate(x))
                .thenCompose(x -> compareWithBaseline(x));

        return result;
    }

    // Run a process template (only enrich status with pid)
    private CompletableFuture<TestStatus> runProcessTemplate(TestStatus status) {
        return CompletableFuture.supplyAsync(() -> {
            // Wait 1 second and return random pid
            status.logInfo("Run process template...");
            sleeper(1000, status);
            status.setPid(String.valueOf(random.nextInt()));
            return status;
        });
    }

    private CompletableFuture<TestStatus> poolUntilOfProcessTemplate(TestStatus status) {
        if (status.isValid()) {
            return CompletableFuture.supplyAsync(() -> {
                // weight random duration (from 1s to 5s) to simulate regular process template status
                status.logInfo("Wait for process template to finish...");
                sleeper(random.nextInt(4000) + 1000, status);
                return status;
            });
        } else {
            return CompletableFuture.completedFuture(status);
        }
    }

    private CompletableFuture<TestStatus> loadProcessTemplate(TestStatus status) {
        if (status.isValid()) {
            return CompletableFuture.supplyAsync(() -> {
                status.logInfo("Load process template result...");
                sleeper(1000, status);

                // Assign fake value as content
                status.setProcessTemplateResult(random.nextInt(2));
                return status;
            });
        } else {
            return CompletableFuture.completedFuture(status);
        }
    }

    private CompletableFuture<TestStatus> compareWithBaseline(TestStatus status) {
        if (status.isValid()) {
            return CompletableFuture.supplyAsync(() -> {
                status.logInfo("Compare with baseline...");
                sleeper(1000, status);

                // raise that a diff has been found if baseline is different from PT result
                if (status.getProcessTemplateResult() != pt.getBaselineContent()) {
                    status.setDiffFound();
                }
                return status;
            });
        } else {
            return CompletableFuture.completedFuture(status);
        }
    }

    private void sleeper(int ms, TestStatus status) {
        try {
            sleep(ms);
        } catch (InterruptedException e) {
            status.setFailed();
        }
    }
}
