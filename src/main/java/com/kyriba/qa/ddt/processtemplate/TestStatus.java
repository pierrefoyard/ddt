package com.kyriba.qa.ddt.processtemplate;

import com.kyriba.qa.ddt.processtemplate.ProcessTemplate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
public class TestStatus {
    public static final String SUCCEED = "SUCCEED";
    private String status = SUCCEED;
    private String pid = "";
    private final ProcessTemplate pt;
    private int processTemplateResult = -1;

    public TestStatus(ProcessTemplate pt) {
        this.pt = pt;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public void setFailed() {
        status = "FAILED";
    }

    public boolean isValid() {
        return status.equals(SUCCEED);
    }

    public void logInfo(String s) {
        log.info("{}: {}", pt.getName(), s);
    }

    public void setProcessTemplateResult(int value) {
        this.processTemplateResult = value;
    }

    public void setDiffFound() {
        logInfo("Regression has been found");
        status = "UNSTABLE";
    }
}
