package com.kyriba.qa.ddt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DdtApplication {
	public static void main(String[] args) {
		ConfigurableApplicationContext run = SpringApplication.run(DdtApplication.class, args);
		run.close();
	}

}
