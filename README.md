# DDT Sample #

This repository has been created to experiment on DDT.

### Goal ###

* Demonstrate that springboot helps for faster development
* Demonstrate that simple algorithm can be applied for DDT
* Demonstrate we can create compiled artifacts better than cloning and recompiling code for each test
* Demonstrate we can dynamically create tests
* Demonstrate how easy it is to use CompletableFuture better than taking care of multi-threading

### What is missing ###

* Demonstrate that REST invokation can be highly simplified with Springboot libraries
